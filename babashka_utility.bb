(ns babashka-utility)

(require '[clojure.string :as str])

(defn ensure-list
  "Wrap in a list if not a list"
  [x]
  (if (seq? x)
    x
    (list x)))

(defn trim-all
  [strs]
  (map str/trim strs))

(defn non-destructive-split
  [in-str regex trim?]
  (let [formatted-str (if trim? (str/trim in-str) in-str)
        regex (re-pattern (str ".*?" regex))
        str-matches (re-seq regex formatted-str)]
    str-matches))

(defn words-equal-any?
  [str sub-strs]
  (some identity
        (map #(some (fn [x]
                      ;(println "str:" % "vs. subs:" x)
                      (= % x))
                    sub-strs)
             (str/split str #" "))))

(defn words-include-any?
  [str sub-strs]
  (some #(str/includes? str %)
        sub-strs))

