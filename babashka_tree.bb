(ns babashka-tree)

(require '[babashka-utility :as util])

(defn count-tree-nodes
  [tree]
  (reduce (fn [sum node]
            (if (seq? node)
              (+ sum (count-tree-nodes node))
              (inc sum)))
          0 tree))

(defn tree->add-branch
  "Add a branch to a tree"
  [tree subtree]
  (conj tree subtree))

(defn tree->add-nodes
  "Add one or multiple nodes to a tree"
  [tree nodes]
  (if (seq? nodes)
    (apply tree->add-branch tree nodes)
    (tree->add-branch tree nodes))) ;;nodes is really just a single node, so just add it

(defn get-branch-contains-helper
  "Get the tree branch that contains the search term"
  [tree terms comparitor]
  (filter seq (map (fn [node]
                       (when (seq? node)
                         (if (some #(when (not (seq? %)) (comparitor % terms)) node)
                           node
                           (get-branch-contains-helper node terms))))
                   tree)))

(defn get-branch-contains
  [tree terms]
  (get-branch-contains-helper tree (util/ensure-list terms) util/words-include-any?))

(defn get-branch-not-contains
  [tree terms]
  (get-branch-contains-helper tree (util/ensure-list terms) (fn [item terms]
                                                              (not (util/words-include-any?
                                                                    item
                                                                    terms)))))

(defn filter-tree-surface
  "1-depth filter of a tree"
  [tree filter-strs]
;;  (println (last tree))
 ;; (println (count tree))
  ;;(println (map identity tree))
  ;;(println (type tree))
  (map #(filter (fn [s]
                  ;;(println s)
                  (when (not (seq? s))
                    (util/words-include-any? s filter-strs))) %) tree))
